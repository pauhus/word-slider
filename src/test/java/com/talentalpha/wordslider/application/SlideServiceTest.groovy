package com.talentalpha.wordslider.application

import com.talentalpha.wordslider.infrastucture.StoreRepository
import spock.lang.Specification

class SlideServiceTest extends Specification {

    private StoreRepository storeRepository
    private SlideService slideService


    void setup() {
        storeRepository = new StoreRepository()
        slideService = new SlideService(storeRepository);
    }

    def "'Process' method should return map of slides"() {
        given: "Input string"

        storeRepository.setStore(storeState)

        when: "process method is invoked"
        def result = slideService.process(input)

        then: "map should be returned'"
        result == expectedResult

        where:
        input                   | storeState | expectedResult

        "Mary went Mary's gone" | Map.of(
                "Mary", 223,
                "Mary gone", 30,
                "Mary's gone", 23,
                "went Mary's", 123,
                "went", 1)                   | Map.of("went Mary's", 123, "Mary", 223)

        "Mary went Mary's gone" | Map.of(
                "Mary", 223,
                "Mary gone", 30,
                "Mary's gone", 23,
                "went Mary's gone", 123,
                "went", 1)                   | Map.of("went Mary's gone", 123, "Mary", 223)

        "Mary went Mary's gone" | Map.of(
                "Mary gone", 30,
                "Mary's gone", 23,
                "Mary went Mary's", 123,
                "gone", 1)                   | Map.of("Mary went Mary's", 123, "gone", 1)

        "Mary went Mary's gone" | Map.of(
                "Mary's gone", 23,
                "Mary went Mary's", 123,
                "Mary went Mary's gone", 30,
                "gone", 1)                   | Map.of("Mary went Mary's gone", 30)

    }

    def "'Process' method should throw exception when empty input is passed"() {
        given: "Empty input string"
        def input = ""

        when: "process method is invoked"
         slideService.process(input)

        then: "exception should be thrown"
        thrown(IllegalArgumentException.class)
    }

    def "'Process' method should throw exception when null input is passed"() {
        given: "Empty input string"
        def input = null

        when: "process method is invoked"
        slideService.process(input)

        then: "exception should be thrown"
        thrown(IllegalArgumentException.class)
    }
}
