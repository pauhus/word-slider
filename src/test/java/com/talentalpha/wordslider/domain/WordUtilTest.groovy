package com.talentalpha.wordslider.domain

import spock.lang.Specification

class WordUtilTest extends Specification {

    def "'PrepareSlides' method should return list of slides"() {
        given: "Input string"
        def text = "Mary went Mary's gone"
        def expectedResult = List.of(
                List.of(new Word("Mary"), new Word("went"), new Word("Mary's"), new Word("gone")),
                List.of(new Word("Mary"), new Word("went"), new Word("Mary's")),
                List.of(new Word("went"), new Word("Mary's"), new Word("gone")),
                List.of(new Word("Mary"), new Word("went")),
                List.of(new Word("went"), new Word("Mary's")),
                List.of(new Word("Mary's"), new Word("gone")),
                List.of(new Word("Mary")),
                List.of(new Word("went")),
                List.of(new Word("Mary's")),
                List.of(new Word("gone")))

        when: "PrepareSlides method is invoked"
        def result = WordUtil.prepareSlides(text)

        then: "list should be returned'"
        result == expectedResult
    }
}
