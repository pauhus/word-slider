package com.talentalpha.wordslider.infrastucture;

import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class StoreRepository {

    private Map<String, Integer> store = new ConcurrentHashMap<>();

    public Integer findValue(String key) {
        if (contains(key)) {
            return store.get(key);
        }
        return null;
    }

    public boolean contains(String key) {
        return store.containsKey(key);
    }

    public void setStore(Map<String, Integer> store) {
        this.store = store;
    }
}
