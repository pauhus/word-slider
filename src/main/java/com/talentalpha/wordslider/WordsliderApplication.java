package com.talentalpha.wordslider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WordsliderApplication {

    public static void main(String[] args) {
        SpringApplication.run(WordsliderApplication.class, args);
    }

}
