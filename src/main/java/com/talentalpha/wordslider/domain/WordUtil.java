package com.talentalpha.wordslider.domain;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class WordUtil {

    public static List<List<Word>> prepareSlides(String text) {
        List<Word> inputWords = extractTextToWords(text);
        List<List<Word>> slides = new ArrayList<>();
        for (int j = 0; j < inputWords.size(); j++) {
            for (int k = 0; k <= j; k++) {
                List<Word> words = new ArrayList<>();
                for (int index = k; index < inputWords.size() - j + k; index++) {
                    words.add(inputWords.get(index));
                }
                slides.add(words);
            }
        }
        return Collections.unmodifiableList(slides);
    }

    public static String joinWords(List<Word> words) {
        return words.stream().map(Word::getContent).collect(Collectors.joining(StringUtils.SPACE));
    }

    private static List<Word> extractTextToWords(String text) {
        if (StringUtils.isEmpty(text)) {
            throw new IllegalArgumentException("Input string cannot be empty!");
        }
        return Arrays.stream(text.split(StringUtils.SPACE)).map(Word::new).collect(Collectors.toList());
    }
}
