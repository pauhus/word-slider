package com.talentalpha.wordslider.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(exclude = "consumed")
@EqualsAndHashCode(exclude = "consumed")
public final class Word {

    private final String content;
    private volatile boolean consumed = false;

    Word(String content) {
        this.content = content;
    }

    public void setConsumed() {
        this.consumed = true;
    }
}
