package com.talentalpha.wordslider.application;

import com.talentalpha.wordslider.domain.Word;
import com.talentalpha.wordslider.domain.WordUtil;
import com.talentalpha.wordslider.infrastucture.StoreRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


@Service
@Log4j2
public class SlideService {

    private static final int THREADS_NO = 10;
    private final StoreRepository storeRepository;

    @Autowired
    public SlideService(final StoreRepository storeRepository) {
        this.storeRepository = storeRepository;
    }

    public Map<String, Integer> process(String text) {
        Map<String, Integer> resultMap = new HashMap<>();
        List<List<Word>> slides = WordUtil.prepareSlides(text);
        slides.forEach(slide -> processSlide(resultMap, slide));
        return resultMap;
    }


    private void processSlide(Map<String, Integer> resultMap, List<Word> words) {
        Optional<Word> anyConsumed = words.stream().filter(Word::isConsumed).findAny();
        if (anyConsumed.isPresent()) {
            return;
        }

        String slide = WordUtil.joinWords(words);

        ExecutorService executorService = Executors.newFixedThreadPool(THREADS_NO);
        Future<Integer> submit = executorService.submit(() -> storeRepository.findValue(slide));

        try {
            Optional.ofNullable(submit.get())
                    .ifPresent(result -> {
                        markAsConsumed(words);
                        resultMap.put(slide, result);
                    });
        } catch (InterruptedException | ExecutionException e) {
            log.error("An error occurred while processing slide: {}", slide);
            e.printStackTrace();
        }
    }

    private void markAsConsumed(List<Word> slides) {
        slides.forEach(Word::setConsumed);
    }
}
