# Getting Started

Project with aim creating Word slides component.

## About project
Project is created with Spring Boot, using libraries like: Lombok, Spock, Apache Commons.

## Java version

**Java 11** has been used.

## Installation

Project can be build with Gradle. 
Just run:

>gradle build


## Description

To realize task **SlideService** has been created, with a method which:
* accepts String parameter - a free-text sentence entered by user (_IllegalArgumentException_ is raised when it's null or empty)
* returns: Map<String, Integer> which is a result of a sliding algorithm

You can find examples about how this algorithm works by looking at **SlideServiceTest** class.
